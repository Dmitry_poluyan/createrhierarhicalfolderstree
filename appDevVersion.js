'use strict';

const fs = require('fs');

var path = './mainDir';

console.log('Start');

fs.mkdirSync(path);

var config = {
    neasting: 2,
    files: 1,
    dirInEachDir: 2,
    text: 10
};

function getText(n){
    var text = '';
    for(var i = 0; i < n; i++){
        text+= 'text';
    }
    return text;
}

function createFiles(path, count, text){
    for(var n = 0; n < count; n++){
        fs.writeFileSync(path +'/file' + n +'.txt', text);
    }
}

function creater(countDirs, countFiles, text, path){


    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;
        fs.mkdirSync(subPath);

        createFiles(subPath, countFiles, text);
        console.log('1' + subPath);

        for(var n = 0; n < countDirs; n++){
            var subPath1 = path + '/' + n;

            if(config.neasting > 0){
                config.neasting --;
                creater(countDirs, countFiles, text, subPath1);
            }else{
                break;
            }
        }
    }
}

creater(config.dirInEachDir, config.files, getText(config.text), path);

console.log('End');
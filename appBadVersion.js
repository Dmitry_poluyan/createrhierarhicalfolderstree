'use strict';

const fs = require('fs');

var path = './mainDir';

console.log('Start');
fs.mkdirSync(path);

var config = {
    neasting: 2,
    files: 1,
    dirInEachDir: 2,
    text: 10 
};

function getText(n){
    var text = '';
    for(var i = 0; i < n; i++){
        text+= 'text';
    }
    return text;
}

function createFiles(path, count, text){
    for(var n = 0; n < count; n++){
        fs.writeFileSync(path +'/file' + n +'.txt', text);
    }
}

createSixNeasting(config.dirInEachDir, config.files, getText(config.text));

function createSixNeasting(countDirs, countFiles, text){
    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;
        fs.mkdirSync(subPath);

        createFiles(subPath, countFiles, text);
    }

    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;

        for(var i1 = 0; i1 < countDirs; i1++){
            var subPath1 = subPath + '/' + i1;
            fs.mkdirSync(subPath1);
            createFiles(subPath1, countFiles, text);
        }
    }

    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;

        for(var i1 = 0; i1 < countDirs; i1++){
            var subPath1 = subPath + '/' + i1;

            for(var i2 = 0; i2 < countDirs; i2++){
                var subPath2 = subPath1 + '/' + i2;
                fs.mkdirSync(subPath2);

                createFiles(subPath2, countFiles, text);
            }
        }
    }

    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;

        for(var i1 = 0; i1 < countDirs; i1++){
            var subPath1 = subPath + '/' + i1;

            for(var i2 = 0; i2 < countDirs; i2++){
                var subPath2 = subPath1 + '/' + i2;

                for(var i3 = 0; i3 < countDirs; i3++){
                    var subPath3 = subPath2 + '/' + i3;
                    fs.mkdirSync(subPath3);

                    createFiles(subPath3, countFiles, text);
                }
            }
        }
    }

    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;

        for(var i1 = 0; i1 < countDirs; i1++){
            var subPath1 = subPath + '/' + i1;

            for(var i2 = 0; i2 < countDirs; i2++){
                var subPath2 = subPath1 + '/' + i2;

                for(var i3 = 0; i3 < countDirs; i3++){
                    var subPath3 = subPath2 + '/' + i3;

                    for(var i4 = 0; i4 < countDirs; i4++){
                        var subPath4 = subPath3 + '/' + i4;
                        fs.mkdirSync(subPath4);

                        createFiles(subPath4, countFiles, text);
                    }
                }
            }
        }
    }

    for(var i = 0; i < countDirs; i++){
        var subPath = path + '/' + i;

        for(var i1 = 0; i1 < countDirs; i1++){
            var subPath1 = subPath + '/' + i1;

            for(var i2 = 0; i2 < countDirs; i2++){
                var subPath2 = subPath1 + '/' + i2;

                for(var i3 = 0; i3 < countDirs; i3++){
                    var subPath3 = subPath2 + '/' + i3;

                    for(var i4 = 0; i4 < countDirs; i4++){
                        var subPath4 = subPath3 + '/' + i4;

                        for(var i5 = 0; i5 < countDirs; i5++){
                            var subPath5 = subPath4 + '/' + i5;
                            fs.mkdirSync(subPath5);

                            createFiles(subPath5, countFiles, text);
                        }
                    }
                }
            }
        }
    }
}

console.log('End');
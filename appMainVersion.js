const fs = require('fs');

var config = {
    level: 3,
    numFoldersl: 2,
    initPath: './mainDir',
    numFiles: 1,
    minFileSize: 10000,
    maxFileSize: 20000
};

function generatePathes(cofig) {
    var temp = [];
    
    for (var c = 0; c < cofig.level; c++) {

        if (temp.length == 0) {

            for (var j = 0; j < cofig.numFoldersl; j++) {
                temp.push(cofig.initPath + '/' + j);
            }
        }else {
            var lent = temp.length;
            var i = Math.pow(cofig.numFoldersl,c - 1) - 1;

            for (i; i < lent; i++) {

                for (var j = 0; j < cofig.numFoldersl; j++) {
                    var item = temp[i] + '/' + j;

                    if (temp.indexOf(item) < 0) {
                        temp.push(temp[i] + '/' + j);
                    }
                }

            }
        }
    }
    return temp;
}

function createFiles(path, count, text) {
    for (var n = 0; n < count; n++) {
        fs.writeFileSync(path + '/file' + n + '.txt', text);
    }
}

function createFolders(pathes, config) {
    fs.mkdirSync(config.initPath);
    createFiles(config.initPath,config.numFiles, getText(getRandomInt(config.minFileSize, config.maxFileSize)));

    for (var i = 0; i < pathes.length; i++) {
        fs.mkdirSync(pathes[i]);
        createFiles(pathes[i],config.numFiles, getText(getRandomInt(config.minFileSize, config.maxFileSize)));
    }
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function getText(n){
    var text = '';
    for(var i = 0; i < n; i++){
        text+= 'text';
    }
    return text;
}

console.log('start');

var a = generatePathes(config);

createFolders(a, config);

console.log('finished');